import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './components';

import './assets/styles/styles.css';


ReactDOM.render(<App />, document.getElementById('root'));
