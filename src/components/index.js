import App from './App';
import Feed from './Feed';
import Instagram from './Instagram';
import Picture from './Picture';
import Navbar from './Navbar';
import Logo from './Logo';

export {
  App, Feed, Instagram, Picture, Navbar, Logo
};
