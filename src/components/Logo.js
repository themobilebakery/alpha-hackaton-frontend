import React from 'react';

import typo from '../assets/img/sc-typo.png';
import cheese from '../assets/img/sc-cheese.png';


const Logo = () => (
  <div className="sc-logo">
    <img
      src={cheese}
      className="sc-logo__cheese"
      alt="A cheese"
    />
    <img
      src={typo}
      className="sc-logo__typo"
      alt="SayChiiis!"
    />
  </div>
);

export default Logo;
