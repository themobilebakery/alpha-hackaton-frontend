import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Logo } from './';

class Navbar extends Component {
  constructor(...args) {
    super(...args);

    this.state = {
      inputValue: ''
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleOnChangeInput = this.handleOnChangeInput.bind(this);
  }

  componentWillMount() {
    this.props.onSearch('happiness');
  }

  handleSubmit(event) {
    event.preventDefault();

    this.props.onSearch(this.state.inputValue);
  }

  handleOnChangeInput(event) {
    this.setState({ inputValue: event.target.value });
  }


  render() {
    return (
      <nav className="nav">
        <div className="container is-flex">
          <Logo />
          <form onSubmit={this.handleSubmit}>
            <input value={this.state.inputValue} onChange={this.handleOnChangeInput} className="nav__input" type="text" placeholder="Buscar" />
            {/* <button>Buscar</button> */}
          </form>
        </div>
      </nav>
    );
  }
}

Navbar.propTypes = {
  onSearch: PropTypes.func.isRequired
};

export default Navbar;
