import React from 'react';
import PropTypes from 'prop-types';

const Picture = (props) => (
  <figure className="picture">
    <img className="picture__img" src={props.img} alt="" />
    <figcaption>
      <div className="picture__description">
        <div className="wrapper">
          <span role="img" aria-label="Anger">{Math.round(props.anger * 100)}% 😡</span>
          <span role="img" aria-label="Contempt">{Math.round(props.contempt * 100)}% 😒</span>
          <span role="img" aria-label="Disgust">{Math.round(props.disgust * 100)}% 🤢</span>
          <span role="img" aria-label="Fear">{Math.round(props.fear * 100)}% 😨</span>
        </div>
        <div className="wrapper">
          <span role="img" aria-label="Happiness">{Math.round(props.happiness * 100)}% 😀</span>
          <span role="img" aria-label="Neutral">{Math.round(props.neutral * 100)}% 😐</span>
          <span role="img" aria-label="Sadness">{Math.round(props.sadness * 100)}% 😞</span>
          <span role="img" aria-label="Surprise">{Math.round(props.surprise * 100)}% 😮</span>
        </div>
      </div>
    </figcaption>
  </figure>
);

Picture.propTypes = {
  img: PropTypes.string.isRequired,
  anger: PropTypes.number.isRequired,
  contempt: PropTypes.number.isRequired,
  disgust: PropTypes.number.isRequired,
  fear: PropTypes.number.isRequired,
  happiness: PropTypes.number.isRequired,
  neutral: PropTypes.number.isRequired,
  sadness: PropTypes.number.isRequired,
  surprise: PropTypes.number.isRequired,
};

export default Picture;
