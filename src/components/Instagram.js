import React from 'react';
import PropTypes from 'prop-types';

import { Feed } from './';

const Instagram = ({ photos, emotion }) => (
  <section className="instagram">
    <div className="container">
      <h1 className="instagram__title">#{emotion}</h1>
      <p className="instagram__subtitle"><strong>{photos.length}</strong> posts</p>
      <Feed photos={photos} />
    </div>
  </section>
);

Instagram.propTypes = {
  photos: PropTypes.arrayOf(PropTypes.object).isRequired,
  emotion: PropTypes.string.isRequired
};

export default Instagram;
