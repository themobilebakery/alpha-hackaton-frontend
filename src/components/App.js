import React, { Component } from 'react';

import { Instagram, Navbar } from './';


class App extends Component {
  constructor(...args) {
    super(...args);

    this.state = {
      photos: [],
      query: ''
    };

    this.handleSearch = this.handleSearch.bind(this);
  }

  handleSearch(query) {
    const HOST = '192.168.0.192:3000';
    const PHOTOS_ENDPOINT = `http://${HOST}/api/Photos/mood?emotion=${query}`;
    this.setState({ query });

    fetch(PHOTOS_ENDPOINT)
      .then(res => res.json())
      .then(res => res.slice(0, 9))
      .then(photos => this.setState({ photos }));
  }

  render() {
    return (
      <div>
        <Navbar onSearch={this.handleSearch} />
        <Instagram photos={this.state.photos} emotion={this.state.query} />
      </div>
    );
  }
}

export default App;
