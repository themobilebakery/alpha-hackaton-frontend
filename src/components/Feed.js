import React from 'react';
import PropTypes from 'prop-types';

import { Picture } from './';

const Feed = ({ photos }) => {
  const HOST = '192.168.0.192:3000';
  const IMG_URL = `http://${HOST}/api/Attachments/photos/download/%ID%.jpg`;
  return (
    <div className="feed" >
      {photos.map(photo => <Picture key={photo.id} img={IMG_URL.replace('%ID%', photo.id)} {...photo} />)}
    </div >
  );
};

Feed.propTypes = {
  photos: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Feed;
